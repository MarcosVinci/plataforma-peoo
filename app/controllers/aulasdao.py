from app.models.aulas import Aula

class AulaDAO():

  def __init__(self, db):
    self.db = db

  
  def cadastrar(self, aula: Aula):
    sql = """
    insert into aulas
    (publicacao, titulo,descricao, link)
    values
    (?, ?, ?, ?);
    """
  
    cursor = self.db.cursor()
    cursor.execute(sql, (
      aula.publicacao,
      aula.titulo,
      aula.descricao,
      aula.link)
    )

    self.db.commit()

    return cursor.lastrowid

  def obter(self, id):
    sql = """
    select * from aulas
    where id = ?;
    """

    cursor = self.db.cursor()
    cursor.execute(sql, (id))

    return cursor.fetchone()

  def listar(self):
    sql = """
    select * from aulas;
    """

    cursor = self.db.cursor()
    cursor.execute(sql)

    return cursor.fetchall()
  
  def atualizar(self, aula: Aula):
    sql = """
    update aula
    set publicacao = ?, set titulo = ?, set descricao = ?
    where id = ?;
    """

    cursor = self.db.cursor()
    cursor.execute(sql, (
      aula.publicacao,
      aula.titulo,
      aula.descricao,
      aula.id)
    )

    self.db.commit()

    return cursor.rowcount
  
  def deletar(self, id):
    sql = """
    delete from aulas
    where id = ?;
    """

    cursor = self.db.cursor()
    cursor.execute(sql, (id))

    self.db.commit()

    return cursor.rowcount 
  