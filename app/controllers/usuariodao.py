from app.models.usuario import Usuario

class UsuarioDAO():

  def __init__(self, db):
    self.db = db

  def cadastrar(self, usuario: Usuario):
    sql = """
    insert into usuarios
    (nome, email,matricula, nivel_acesso)
    values
    (?, ?, ?, ?);
    """
  
    cursor = self.db.cursor()
    cursor.execute(sql, (
      usuario.nome,
      usuario.email,
      usuario.matricula,
      usuario.nivel_acesso
    )
    )
    self.db.commit()

    return cursor.lastrowid

  def verificar_existencia_usuario(self, matricula):
    sql = """
    select *
    from usuarios
    where matricula = ?;
    """

    cursor = self.db.cursor()
    cursor.execute(sql, (matricula,))

    return cursor.fetchone()
