from flask import render_template, request, url_for, redirect, session, flash, jsonify
from sqlite3.dbapi2 import Error, complete_statement, connect
from authlib.integrations.flask_client import OAuth
from datetime import datetime

from requests.api import get
from app import app
import os, hashlib
from app.config import GOOGLE_KEY, GOOGLE_USERID

from app.models.connection import get_db
from app.services.SuapApi import Suap

from app.models.usuario import Usuario
from app.controllers.usuariodao import UsuarioDAO
from app.models.aulas import Aula
from app.controllers.aulasdao import AulaDAO

@app.route('/')
def signin_suap():
  return render_template('sign-in.html') 

@app.route('/auth/suap', methods=['POST',])
def auth_suap():
  if( request.method == "POST" ):
    # Captando as informações enviadas pelo usuario
    matriculation = request.form.get('Matricula')
    password = request.form.get("Senha")

    # Verificando se todos os campos foram preenchidos
    if( matriculation and password ):
      api = Suap() # Instanciando a classe Suap
      user_auth_suap = api.autentica(matriculation, password)

      # Se existir um usuario com essas credenciais
      if( user_auth_suap is not None ):
        # Instanciando o controlador de usuarios
        userdao = UsuarioDAO(get_db())

        user = userdao.verificar_existencia_usuario(user_auth_suap['matricula'])

        if( user is None ):
          print(user_auth_suap)

          # Criando uma instancia de usuario
          user_instance = Usuario(
            nome=user_auth_suap['vinculo']['nome'],
            email=user_auth_suap['email'],
            matricula=user_auth_suap['matricula']
          )

          # Verificando se o usuario existe
          try:
            user_id = userdao.cadastrar( user_instance )
          except:
            flash('Esse E-mail e/ou matricula já estão sendo utilizados!', 'danger')
            return redirect(url_for("index"))

        # Salvando os dados do usuario na sessao
        session['logged'] = True 
        session['infos'] = {
          'nome': user_auth_suap['vinculo']['nome'],
          'matricula': user_auth_suap['matricula'],
          'tipo_vinculo': user_auth_suap['tipo_vinculo'],
          'nome_usual': user_auth_suap['nome_usual']
        }
        
        #fazendo ainda...
        return redirect(url_for('painel'))
        
      else:
        flash("Matricula e/ou senha incorretos!", "danger")
        return redirect(url_for("signin_suap"))

    else:
      flash("Preencha todos os campos!", "warning")
      return redirect(url_for("signin_suap"))

@app.route('/painel')
def painel():
  if( session.get('logged') == True and session.get('infos') is not None ):
    return render_template('painel.html', infos=session['infos'])
  
  flash('danger','Você não tem permissão para acessar essa funcionalidade, pois não está logado.')
  return redirect(url_for('signin_suap')) 

@app.route('/aulas/cadastrar', methods=['POST',])
def cadastro_aulas():
  if( request.method == "POST" ):
    # Captando as informações enviadas pelo usuario
    publicacao = request.form.get('Publicacao')
    titulo = request.form.get("Titulo")
    desc = request.form.get("Descricao")
    link = request.form.get("Link")
  
  auladao = AulaDAO(get_db())
  aula = Aula(publicacao,titulo,desc,link)
  aula_id = auladao.cadastrar(aula)
  
  return render_template('painel.html')

@app.route('/aulas/listar')
def listar_aulas():
  return render_template('listar-todas-aulas.html')

@app.route('/desempenho')
def desempenho():
  return render_template('desempenho.html')

@app.route('/materiais')
def listar_materiais():
  return render_template('listar-materiais.html')

@app.route('/logout')
def logout():
  session['logged'] = None
  session.clear()
  
  return redirect('/') # Redirecionando o usuário para a rota "/"
