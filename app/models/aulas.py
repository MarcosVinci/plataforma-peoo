class Aula():
    def __init__(self, publicacao, titulo, descricao, link):
       self.id = 0 
       self.publicacao = publicacao
       self.titulo = titulo
       self.descricao = descricao
       self.link = link


    def getId(self):
        return self._id
    
    def setId(self, id):
        self._id = id
    
    def getAulaId(self):
        return self.id
    
    def setAulaId(self, aula_id):
        self.id = aula_id

    id = property(fget=getId, fset=setId)
    usuario_id = property(fget=getAulaId, fset=setAulaId)